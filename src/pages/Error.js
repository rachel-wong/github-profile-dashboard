import React from 'react';
import styled from 'styled-components';
import { Link, useHistory } from 'react-router-dom';
import ErrorImage from '../images/error.svg'

const Error = () => {
  return (
    <Wrapper>
      <img src={ErrorImage} className="error-image" />
      <h2>Page not found</h2>
      <Link to="/" exact className="btn">Go back home</Link>
    </Wrapper>
  )
};
const Wrapper = styled.section`
  min-height: 100vh;
  display: grid;
  place-items: center;
  background: var(--clr-primary-10);
  text-align: center;
  h1 {
    font-size: 10rem;
  }
  h3 {
    color: var(--clr-grey-3);
    margin-bottom: 1.5rem;
  }
`;
export default Error;
