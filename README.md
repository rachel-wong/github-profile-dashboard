## Github profiler dashboard

> A simple app exercise to search for github users and reveal their commit history stats, tech stack, profile and follower counts.

The purpose of this app is primarily to practice foundational concepts behind lifting global state above component level and configure children components to take down state, as oppose to prop drilling.

### Packages and Libraries used
* Fusion charts
* React icons
* Github api

To work on netlify, redirects was added to public folder.

### Components
Aside from the pages for dashboard, 404 etc, the application consisted of a series of child components all embedded in the Dashboard page via the Repos component.

* Search: A searchbar that activates a keyword search for a github username on enter keypress. The search function is passed down from global context.
* Info: a profiler that displays the user's key details: follower count, tallies in number of repositories, github gists, following and social media links. It displays this information through sub-child components Followers and Child,
* Bar3D: Two 3D bar charts to display most used languages by forks.
* PieChart: A chart displaying most starred popular languages.
* Doughnut chart: A similar chart but with slightly different configuration, the doughnut chart displays a user's most used programming language.

All charts are powered by the fusion charts open source library, and pull data from global context via the Repos component.

### Extension
This project sets the scene for further extension to improve the user experience including:

* Error message: pop up modal to display when a username cannot be located. This component could be embedded in the Dashboard route and draws the appropriate error message and a boolean value from the context state to trigger appearance.
* Authentication: protected routes could be used to allow users to first log in before begin searching. This would be accompanied by error page for unauthorised access.