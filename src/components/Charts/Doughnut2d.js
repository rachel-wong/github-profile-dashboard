import React from 'react';
import FusionCharts from "fusioncharts";
import ReactFC from "react-fusioncharts";
import Doughnut2D from "fusioncharts/fusioncharts.charts";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.candy";

ReactFC.fcRoot(FusionCharts, Doughnut2D, FusionTheme);

const Doughnut2d = ({ chartdata }) => {

  const chartConfigs = {
    type: "doughnut2d",
    width: "500",
    height: "500",
    dataFormat: "json",
    dataSource: {
      chart: {
        caption: "Most used languages by Stars",
        theme: "candy",
        doughnutRadius: '45%',
        showPercentValues: 0,
      },
      data: chartdata
    }
  };
  return (
    <div>
      <ReactFC {...chartConfigs} />
    </div>
  )
};

export default Doughnut2d;
