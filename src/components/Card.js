import React, { useContext} from 'react';
import { GithubContext } from '../context/context';
import styled from 'styled-components';
import { MdBusiness, MdLocationOn, MdLink } from 'react-icons/md';
import GithubProfilePlaceholder from '../images/android-chrome-192x192.png'

const Card = () => {

  const { githubUser } = useContext(GithubContext);

  return (
    <>
      <Wrapper>
        {Object.keys(githubUser).length != 0 ? (
          <>
            <header>
              <img src={githubUser.avatar_url ? githubUser.avatar_url : GithubProfilePlaceholder} alt={"Image of " + githubUser.login} />
              <h4>{githubUser.name ? githubUser.name : githubUser.login}</h4>
              { githubUser.login && <p>{"@" + githubUser.login}</p>}
              { githubUser.html_url && <a href={ githubUser.html_url }>Follow</a> }
            </header>
            <p>{githubUser.bio}</p>
            <div className="links">
              <p><MdLocationOn />{ githubUser.location ? githubUser.location : "somewhere on earth"}</p>
              <a href={ githubUser.blog || ""}><p><MdLink />{ githubUser.blog ? githubUser.blog : "No blog"}</p></a>
              <a href={ githubUser.html_url ? githubUser.html_url : "http://www.github.com" } target="_blank" ><p><MdBusiness />{ githubUser.company ? githubUser.company : "No company"}</p></a>
            </div>
          </>
        ) : (
            <header className="no-user">
              <img src={GithubProfilePlaceholder} alt="Github profiler" />
              <h3>No user found yet</h3>
            </header>
          ) }
      </Wrapper>
    </>
  )
};

const Wrapper = styled.article`
  background: var(--clr-white);
  padding: 1.5rem 2rem;
  border-top-right-radius: var(--radius);
  border-bottom-left-radius: var(--radius);
  border-bottom-right-radius: var(--radius);
  position: relative;
  &::before {
    content: 'user';
    position: absolute;
    top: 0;
    left: 0;
    transform: translateY(-100%);
    background: var(--clr-white);
    color: var(--clr-grey-5);
    border-top-right-radius: var(--radius);
    border-top-left-radius: var(--radius);
    text-transform: capitalize;
    padding: 0.5rem 1rem 0 1rem;
    letter-spacing: var(--spacing);
    font-size: 1rem;
  }
  header {
    display: grid;
    grid-template-columns: auto 1fr auto;
    align-items: center;
    column-gap: 1rem;
    margin-bottom: 1rem;
    img {
      width: 75px;
      height: 75px;
      border-radius: 50%;
    }
    h4 {
      margin-bottom: 0.25rem;
    }
    p {
      margin-bottom: 0;
    }
    a {
      color: var(--clr-primary-5);
      border: 1px solid var(--clr-primary-5);
      padding: 0.25rem 0.75rem;
      border-radius: 1rem;
      text-transform: capitalize;
      letter-spacing: var(--spacing);
      transition: var(--transition);
      cursor: pointer;
      &:hover {
        background: var(--clr-primary-5);
        color: var(--clr-white);
      }
    }
  }
  .no-user {
    display:flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
  .bio {
    color: var(--clr-grey-3);
  }
  .links {
    p,
    a {
      margin-bottom: 0.25rem;
      display: flex;
      align-items: center;
      svg {
        margin-right: 0.5rem;
        font-size: 1.3rem;
      }
    }
    a {
      color: var(--clr-primary-5);
      transition: var(--transition);
      svg {
        color: var(--clr-grey-5);
      }
      &:hover {
        color: var(--clr-primary-3);
      }
    }
  }
`;
export default Card;
