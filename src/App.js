import React from 'react';
import { Dashboard, Login, PrivateRoute, AuthWrapper, Error } from './pages';
import { BrowserRouter as Router, Switch, Redirect, Route } from 'react-router-dom';
import { GithubProvider } from './context/context'

function App() {
  return (
    <GithubProvider>
      <Router>
        <Switch>
          <Route path="/" exact component={Dashboard} />
          <Route path="/login" component={ Login } />
          <Route path="/404" component={Error} />
          <Redirect from="*" to="/404" />
        </Switch>
      </Router>
    </GithubProvider>
  );
}

export default App;
