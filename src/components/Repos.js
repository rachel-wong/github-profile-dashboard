import React, { useContext } from 'react';
import styled from 'styled-components';
import { GithubContext } from '../context/context';
import { Pie3d, Column3d, Bar3d, Doughnut2d } from './Charts';

const Repos = () => {

  const { repos } = useContext(GithubContext);

  // ** Top 5 popular languages ** //
  // starting of the object looks like { "javascript": 1, "html": 12, "css": 30 }
  // // total is what we're returning, item is iterator
  let languages = repos.reduce((total, item) => {
    const { language, stargazers_count } = item; // pulls out the 'language' instances
    if (!language) return total; // removes the null values out of the calculation

    if (!total[language]) {
      // language does not exist, create a new object and assign value to it
      total[language] = { label: language, value: 1 , stars: stargazers_count }
    } else {
      total[language] = {...total[language], value: total[language].value + 1, stars: total[language].stars + stargazers_count}
    }

    return total
  }, {}) // empty object is what we're trying to return from the .reduce

  // converts object into an array of objects
  // // sort by "value" property
  // // shorten array to first five
  const mostUsed = Object.values(languages).sort((a, b) => {
    return b.value - a.value // sort languages by frequency in descending order
  }).slice(0, 5)

  const mostStars = Object
    .values(languages).sort((a, b) => {
    return b.stars - a.stars // sort languages by stars in descending order
  }).map((item) => {
    return {...item, value: item.stars }
  }).slice(0, 5) // swap out the "value" property for the "stars"

  // stars, forks - identifying repos with the most amount of stars or forks
  const { stars, forks } = repos.reduce((total, item) => {
    const { stargazers_count, name, forks } = item;

    // sets the star/forks count value as the property in an object
    // to which an object is assigned the repo name and stars count
    total.stars[stargazers_count] = { label: name, value: stargazers_count }
    total.forks[forks] = { label: name, value: forks }
    return total
   }, {
    stars: {},
    forks: {} // specifying that the end result with be an object with two nested objects for stars and forks
  })

  // reusable function to format stars and forks as top 5 chart readable objects
  function formatData(object) {
    let array = []
    array = Object.values(object).sort((a, b) => {
      return b.value - a.value
    }).slice(-5).reverse()
    return array
  }

  const top5StarsRepo = formatData(stars)
  const top5ForksRepo = formatData(forks)

  if (Object.keys(repos).length == 0) {
    return (
      <NoChartsWrapper className="section-center">
        <div className="no-charts__content">
          <h3>No repository data to display yet.</h3>
          <p>Either search for another user or this user has not yet created any repositories in their account.</p>
        </div>
      </NoChartsWrapper>
    )
  }

  return (
    <Wrapper className="section-center fusioncharts-container">
      <Bar3d chartdata={ top5ForksRepo }/>
      <Column3d chartdata={ top5StarsRepo }/>
      <Doughnut2d chartdata={ mostUsed }/>
      <Pie3d chartdata={mostStars} />
    </Wrapper>
  )
};

const Wrapper = styled.div`
  display: grid;
  justify-items: center;
  gap: 2rem;
  margin-top: 2rem;

  @media (min-width: 800px) {
    grid-template-columns: 1fr 1fr;
  }

  @media (min-width: 1200px) {
    grid-template-columns: 2fr 3fr;
  }

  div {
    width: 100% !important;
  }

  .fusioncharts-container {
    width: 100% !important;
    background: pink;
  }

  .no-charts {
    background: pink;
  }
  svg {
    width: 100% !important;
    border-radius: var(--radius) !important;
  }
`;

const NoChartsWrapper = styled.div`
  display:flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 30rem;

  .no-charts__content{
    width: 100%;
    display: block;
    text-align: center;
  }
`

export default Repos;
