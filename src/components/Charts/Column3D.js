import React from 'react';
import FusionCharts from "fusioncharts";
import ReactFC from "react-fusioncharts";
import Column3D from "fusioncharts/fusioncharts.charts";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.candy";
ReactFC.fcRoot(FusionCharts, Column3D, FusionTheme);

const Column3d = ({ chartdata }) => {

  const chartConfigs = {
    type: "column2d",
    width: "100%",
    height: "400",
    dataFormat: "json",
    dataSource: {
      chart: {
        caption: "Most popular languages by Stars",
        theme: "candy",
        yAxisName: "Stars",
        xAxisName: "Repos",
        xAxisNameFontSize: '16px',
        yAxisNameFontSize: '16px'
      },
      data: chartdata
    }
  };
  return <ReactFC {...chartConfigs} />
};

export default Column3d;
