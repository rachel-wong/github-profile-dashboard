import React, { useContext } from 'react';
import { Info, Repos, User, Search, Navbar } from '../components';
import loadingImage from '../images/preloader.gif';
import { GithubContext } from '../context/context';

const Dashboard = () => {

  const { loading } = useContext(GithubContext);

  // can set up a separate error handling for loading instead of relying on ternary check statements
  if (loading) {
    return (
      <main>
        <Navbar />
        <Search />
        <img src={ loadingImage } className="loading-img" alt="Loading your data now" />
      </main>
    )
  }
  return (
    <main>
      <Navbar />
      <Search />
      <Info />
      <User />
      <Repos />
    </main>
  );
};

export default Dashboard;
