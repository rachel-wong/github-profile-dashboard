import React from 'react';
import FusionCharts from "fusioncharts";
import ReactFC from "react-fusioncharts";
import Bar3D from "fusioncharts/fusioncharts.charts";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
ReactFC.fcRoot(FusionCharts, Bar3D, FusionTheme);

const Bar3d = ({ chartdata }) => {

  const chartConfigs = {
    type: "bar2d",
    width: "100%",
    height: "400",
    dataFormat: "json",
    dataSource: {
      chart: {
        caption: "Most popular languages by Forks",
        theme: "fusion",
        yAxisName: "Forks",
        xAxisName: "Repos",
        xAxisNameFontSize: '16px',
        yAxisNameFontSize: '16px'
      },
      data: chartdata
    }
  };
  return <ReactFC {...chartConfigs} />
};

export default Bar3d