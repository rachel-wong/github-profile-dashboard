import React, { useState, useEffect, createContext, useReducer } from 'react';
import mockUser from './mockData.js/mockUser';
import mockRepos from './mockData.js/mockRepos';
import mockFollowers from './mockData.js/mockFollowers';
import axios from 'axios';

const rootUrl = 'https://api.github.com';

const GithubContext = createContext();

const GithubProvider = ({ children }) => {

  // setting mocks to be the default initial State (default initial github user's dashboard data)
  const [githubUser, setGithubUser] = useState({})
  const [repos, setRepos] = useState([])
  const [followers, setFollowers] = useState([])
  const [requests, setRequests] = useState(0) // requests that can still be made to the github api
  const [loading, setLoading] = useState(true) // it IS loading by default until data arrives

  // two possible error cases (once logged in) 1. no requests, 2. no user name match
  // not null because here is where we declare the shape of the error (is there an error and what is the message?)
  const [error, setError] = useState({show: false, msg: ""});

  const getUser = async (user) => {
    const userResponse = await axios
      .get(rootUrl + "/users/" + user)
      .catch(err => console.error(error))

    // check if there a github-user is found
    if (userResponse) {
      setGithubUser(userResponse.data);
      const { login, followers_url } = userResponse.data

      // it is not necessary to use the paged api endpoint, can use the repos_url or the followers_url just the same ;-)
      await Promise.allSettled([
        axios.get(`${rootUrl}/users/${login}/repos?per_page=100`),
        axios.get(`${followers_url}?per_page=100`)])
        .then((res) => {
          const fulfilled = 'fulfilled'
          const [ repos, followers ] = res // the order of the destructuring matters. it should follow the order of the api calls in the promise allsettled. the naming also matters
          if (repos.status === fulfilled && followers.status === fulfilled) {
            setRepos(repos.value.data)
            setFollowers(followers.value.data)
          }
        })
        .catch(err => console.error(err))
    } else {
      toggleError(true, "No user found" )
    }
    checkRequests()
    setLoading(false)
  }

  const checkRequests = async () => {
    const { data } = await axios.get(rootUrl + "/rate_limit");
    setRequests(data)
    // error handling for when there are no requests remaining
    if (data.rate.remaining === 0) {
      toggleError(true, `Unable to search, please try again after ${new Date(data.rate.reset).toLocaleTimeString()}`)
    }
    setLoading(false)
  }

  // reusable function to handle any errors and display notifications
  // // can set default values to the arguments show, msg if any of these values were not populated at function call
  const toggleError = (show = false, msg = '') => {
    setError({ show, msg })
  }

  useEffect(() => {
    checkRequests() // first thing to run for on page load
  }, [])

  return (
    // whatever that is passed in through the value prop in the Provider will be accessible everywhere in the app
    <GithubContext.Provider
      value={{ githubUser, repos, followers, requests, loading, error, getUser }}>
      {children}
    </GithubContext.Provider>
  )
}

export { GithubProvider, GithubContext }