import React from 'react';
import FusionCharts from "fusioncharts";
import ReactFC from "react-fusioncharts";
import Pie3D from "fusioncharts/fusioncharts.charts";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
ReactFC.fcRoot(FusionCharts, Pie3D, FusionTheme);

const Pie3d = ({ chartdata }) => {

  const chartConfigs = {
    type: "pie3d", // The chart type
    width: "700", // Width of the chart
    height: "400", // Height of the chart
    dataFormat: "json", // Data type
    dataSource: {
      chart: {
        caption: "Most popular languages by Stars",    //Set the chart caption
        theme: "fusion"  //Set the theme for your chart
      },
      data: chartdata
    }
  };

  return <ReactFC {...chartConfigs} />

};

export default Pie3d;
